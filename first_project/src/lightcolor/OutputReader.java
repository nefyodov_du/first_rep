package lightcolor;

import java.io.*;

/**
 * Created by Nefyodov on 02.12.14.
 */
public class OutputReader {
    public static void write() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String path = reader.readLine();
        PrintWriter write = new PrintWriter(new BufferedWriter(new FileWriter("output.html")));

        write.println(InputReader.read(path));
        write.close();

   }
}
