package lightcolor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Nefyodov on 02.12.14.
 */

public class InputReader {
    public static String read (String filename) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        StringBuilder sb = new StringBuilder();

        String s;
        while ((s = reader.readLine()) != null)
            sb.append(s).append("\n");

        reader.close();

        return sb.toString();

    }
}
