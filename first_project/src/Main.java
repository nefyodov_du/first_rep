
import lightcolor.InputReader;
import lightcolor.OutputReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {

    public static void main(String[] args) throws IOException {

        OutputReader.write();

        System.out.println(InputReader.read("output.html"));

    }
}
